function getRandomColor(){
  var letters ="0123456789abcdef";
  var result ="#";

  for(var i=0; i<6;i++){
    result += letters.charAt(parseInt(Math.random() * letters.length));
  }
  return result;
}

window.onload = function(){
  var maxDiam = 50;
  var circleNum = 50;
  var container = $("#container");
  var containerWidth = container.width();
  var containerHeight = container.height();

  $(document).ready(function() {
    for (var i = 0; i < circleNum; i++) {
      var newCircle = $("<div />");
      var d =maxDiam;
      var xPos = Math.random() * Math.max(containerWidth - d - 4, 0);
      var yPos = Math.random() * Math.max(containerHeight - d - 4, 0);
      newCircle.addClass("circle");

      newCircle.css({
        width: d,
          height: d,
          left: xPos,
          top: yPos,
          backgroundColor: getRandomColor()
      });
      container.append(newCircle);
    };
  });


  $(document).ready(function() {
    $('.circle').click(function(){
      var topZ = 0;
      $('.circle').each(function(){
        var thisZ = parseInt($(this).css('z-index'), 10);
        if (thisZ > topZ){
          topZ = thisZ;
        }
      });
      $(this).css('z-index', topZ+1);
    });
  });

  $(document).ready(function() {
    $('.circle').dblclick(function(){
      $(this).remove();
    });
  });
};

$(document).ready(function() {
  $('#addCircle').click(function(){
    var maxDiam = 50;
    var container = $("#container");
    var containerWidth = container.width();
    var containerHeight = container.height();
    var newCircle = $("<div />");
    var d =maxDiam;
    newCircle.addClass("circle");
    newCircle.css({
      width: d,
      height: d,
      left: Math.random() * Math.max(containerWidth - d - 4, 0),
      top: Math.random() * Math.max(containerHeight - d - 4, 0),
      backgroundColor: getRandomColor()
    });
    $("#container").append(newCircle);
    $(document).ready(function() {
      $('.circle').click(function(){
        var topZ = 0;
        $('.circle').each(function(){
          var thisZ = parseInt($(this).css('z-index'), 10);
          if (thisZ > topZ){
            topZ = thisZ;
          }
        });
        $(this).css('z-index', topZ+1);
      });
    });
    $(document).ready(function() {
      $('.circle').dblclick(function(){
        $(this).remove();
      });
    });
  });
});

$(document).ready(function() {
  $('#changeColors').click(function(){
    $(".circle").each(function(){
      $(this).css('background-color', getRandomColor());
    });
  });
});

$(document).ready(function() {
  $('#reset').each(function(){
    $(this).on("click", function(){
      var myNode = document.getElementById("container");
      while(myNode.firstChild){
        myNode.removeChild(myNode.firstChild);
      }
      $(this).queue(window.onload);
      $(this).dequeue();
    });
  });
});

var intervals = [];
$(document).ready(function(){
  $('#start').click(function(){
    $(".circle").each(function(){
      var that = this;

      var left = Math.random() < 0.5 ? true : false;
      intervals.push(setInterval(function(){
        if($(that).position().left > $("#container").position().left && left == true){
          left = true;
          $(that).css({
            left: $(that).position().left - 10 + "px",
          });
        }else if( $(that).position().left + $(that).width()  < $("#container").position().left + $("#container").width() - 20){
          left = false;
          $(that).css({
            left: $(that).position().left + 10 + "px"
          });
        }
        else{
          left = true;
        }
        $(that).css("background-color", getRandomColor());
        $(that).css("border-color", getRandomColor());
      },75));
    });
  });
  
  $("#stop").click(function(){
    intervals.forEach(clearInterval);
  intervals.length = 0;
  });
});
