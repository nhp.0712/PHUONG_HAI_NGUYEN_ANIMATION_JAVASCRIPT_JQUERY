1.  Create a single HTML document named “index.html”.

2.  Create a single, external stylesheet that will be used in the HTML document with an appropriate name.
All CSS written by you should be in this stylesheet.
There is to be no inline or document level CSS for this
assignment. If you are using a framework (like Bootstrap) for styling, it is OK to have their CSS/JavaScript in your project as well.

3.  Create a single, external JavaScript file that will be used in the HTML document with an appropriate name.
All JavaScript should be in this file. There is to be no inline or document level JavaScript for this assignment.

4.  Build a HTML page using jQuery and JavaScript that that is capable of displaying and manipulating a single area that is filled with multi-colored circles. I suggest that the main display area be a separate
"div". Also, each circle should be its own "div". The use of jQuery suggests that you should use jQuery function calls,
selectors, and the general jQuery style (e.g. using anonymous functions). The following requirements must be met:

. Display Area:
The area that displays all circles is to be a reasonable height and width and have a 2px border that is solid black. This area and be centered in the main content area of the page.

. Page Load:
When  the  page  is  loaded,  50  circles  that  are  50px  by  50px  in  size  must  automatically display. These squares must be displayed at random positions within the display area and be colored
randomly. The following function returns a random hexadecimal value and may be used as a starting point for generating a random color:

. Add Circle Button:
This button should add a single circle block to the display area that is 50px wide, 50px high, and has a random background color

. Change Colors Button:
This button should randomly change the color of each circle. In order to obtain an array or list of all circles within the display area, I suggest looking up the function document.querySelectorAll
or using the appropriate jQuery selectors. Note that the selector used in this function can be any selector typically used in CSS. This would allow you to select all circles at one time.

. Reset Button:
This button should remove all circles from the display area and then recreate 50 circles that are 50px by 50px in size must automatically display.  These circles must be displayed at random
positions and be colored randomly. In order to remove all circles from the display area, the following code snippet may be used as a starting point:

. Single Click:
If a circle is clicked once, it should be brought to the foreground (i.e. be displayed above any other squares that are touching it)

. Double Click:
If a circle is clicked twice, it should be removed from the display.

. Start/Top Animation Button:
Add  a  button  that  starts/stops  animation  of  the  circles.   Animation should consist of circles randomly changing colors and randomly floating back and forth within the
display area.  Animation must be implemented using the setInterval function.  During animation, if a square runs into an edge, it must:
a)  Change directions appropriately and continue moving;
b)  Randomly change its background color
c)  Randomly change its border color